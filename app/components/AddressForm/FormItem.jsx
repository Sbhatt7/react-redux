import React, { memo } from 'react';
import { Form, Input } from 'antd';
import styled from 'styled-components';

const StyledFormItem = styled(Form.Item)`
  margin-bottom: 0;
`;

export function injectData(data, form) {
  return memo(({ label, name }) => (
    <>
      <Form.Item label={label}>
        {form.getFieldDecorator(name, {
          initialValue: data[name] || '',
        })(<Input placeholder="" />)}
      </Form.Item>
    </>
  ));
}

export default function FormItem({ name, label, data, form }) {
  return (
    <>
      <StyledFormItem label={label}>
        {form.getFieldDecorator(name, {
          initialValue: data[name] || '',
        })(<Input placeholder="" />)}
      </StyledFormItem>
    </>
  );
}
