import styled from 'styled-components';
import { Divider } from 'antd';

export default styled(Divider)`
  margin: ${props => props.theme.spacing(3)} 0;
  margin-left: -${props => props.theme.spacing(3)};
  width: calc(100% + ${props => props.theme.spacing(6)});
`;
