import { Form, Button, Input } from 'antd';
import { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { ConfirmActionButton } from 'components/Buttons';
import { useTheme } from 'styled-components';
import Float from 'components/Float';
import StyledDivider from 'components/AddressForm/StyledDivider';

const AddressForm = ({
  form,
  data,
  canDelete,
  canSave,
  onDeleteAddress,
  onSaveAddress,
  onCancel,
}) => {
  const theme = useTheme();

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };

  const labels = {
    contact: t('addresses.contact'),
    company: t('addresses.company'),
    street: t('addresses.street'),
    zipCode: t('addresses.zipCode'),
    city: t('addresses.city'),
    country: t('addresses.country'),
    // telephone: t('addresses.telephone'),
    // email: t('addresses.email'),
  };
  const emptyAddress = {
    contact: '',
    company: '',
    street: '',
    zipCode: '',
    city: '',
    country: '',
    telephone: '',
    email: '',
  };

  const [myData, setMyData] = useState(emptyAddress);

  // updata data in modal, each time the data prop changes
  useEffect(() => {
    setMyData({ ...emptyAddress, ...data });
  }, [data]);

  const escFunction = useCallback(event => {
    if (event.key === 'Escape') {
      onCancel();
    }
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false);
    return () => {
      document.removeEventListener('keydown', escFunction, false);
    };
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        values.id = myData.id;
        values.key = myData.key;
        onSaveAddress(values);
      }
    });
  };

  const handleDelete = () => {
    onDeleteAddress(myData);
  };

  const handleCancel = () => {
    return onCancel && onCancel();
  };

  // const FormItem = injectData(data, form);

  const generateFormItems = () =>
    Object.keys(myData).map(name => {
      const label = labels[name];
      // don't return a form item for elements that are not in the label list
      if (!label) return false;
      return (
        <Form.Item key={label} label={label} style={{ margin: 0 }}>
          {form.getFieldDecorator(name, {
            initialValue: myData[name] || '',
          })(<Input placeholder="" />)}
        </Form.Item>
      );
    });

  return (
    <>
      <Form
        {...formItemLayout}
        onSubmit={handleSubmit}
        className="alberich-address-form"
        style={{ minWidth: 400 }}
      >
        {generateFormItems()}
        <StyledDivider />
        <Float>
          <Float.Left>
            <ConfirmActionButton
              type="danger"
              icon="delete"
              message="Adresse wirklich löschen?"
              onConfirm={handleDelete}
              disabled={!canDelete}
            />
          </Float.Left>
          <Float.Right>
            <Button
              onClick={handleCancel}
              style={{ marginRight: theme.spacing(2) }}
            >
              Abbrechen
            </Button>
            <Button type="primary" htmlType="submit" disabled={!canSave}>
              Speichern
            </Button>
          </Float.Right>
        </Float>
      </Form>
    </>
  );
};

AddressForm.propTypes = {
  data: PropTypes.exact({
    contact: PropTypes.string,
    company: PropTypes.string,
    street: PropTypes.string,
    zipCode: PropTypes.string,
    city: PropTypes.string,
    country: PropTypes.string,
    telephone: PropTypes.string,
    email: PropTypes.string,
    key: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
  }),
  onDeleteAddress: PropTypes.func,
  onSaveAddress: PropTypes.func,
  onCancel: PropTypes.func,
  canDelete: PropTypes.bool,
  canSave: PropTypes.bool,
};

AddressForm.defaultProps = {
  canDelete: true,
  canSave: true,
};

export default Form.create({ name: 'address_form' })(AddressForm);
