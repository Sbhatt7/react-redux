import { Popover, Button, Input, AutoComplete, Row, Col } from 'antd';
import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';

const StyledPopover = styled(Popover)`
  .input-button-bar .ant-input-suffix {
    right: 0 !important;
  }
`;

const AutoCompleteInputPopover = ({ onSubmit, suggestions, children }) => {
  const [isPopoverVisible, setIsPopoverVisible] = useState(false);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  const inputDOM = useRef(null);

  useEffect(() => {
    if (isPopoverVisible && inputDOM.current) {
      inputDOM.current.focus();
    }

    if (isPopoverVisible && !isDropdownVisible) {
      // use setTimeout to prevent wrong positioning of dropdown list
      setTimeout(() => setIsDropdownVisible(true), 50);
    }
  }, [isPopoverVisible]);

  suggestions.forEach(s => {
    s.id = String(s.id);
  });

  // TODO: correctly handle enter key
  const handleButtonClick = val => {
    setIsPopoverVisible(false);

    if (suggestions && typeof val !== 'object') {
      onSubmit(val);
    } else if (!suggestions) {
      onSubmit(val);
    }
  };

  const renderSuggestion = sug => (
    <AutoComplete.Option
      key={sug.id}
      id={sug.id}
      name={sug.name}
      date={sug.createdAt}
    >
      <Row>
        <Col span={6}>
          <b>{sug.id}</b>
        </Col>
        <Col span={10}>{sug.name}</Col>
        <Col span={8} style={{ textAlign: 'end' }}>
          {new Date(Date.parse(sug.createdAt)).toLocaleDateString('de-DE', {
            dateStyle: 'medium',
          })}
        </Col>
      </Row>
    </AutoComplete.Option>
  );

  return (
    <>
      <StyledPopover
        placement="right"
        trigger="click"
        visible={isPopoverVisible}
        onVisibleChange={setIsPopoverVisible}
        content={
          <AutoComplete
            autoFocus
            // defaultOpen
            open={isPopoverVisible ? isDropdownVisible : false}
            ref={inputDOM}
            dataSource={suggestions.map(renderSuggestion)}
            onSelect={handleButtonClick}
            optionLabelProp="id"
            filterOption={(inputValue, option) => {
              const inpt = String(inputValue).toLowerCase();
              return (
                String(option.props.id).startsWith(inpt) ||
                option.props.date.toLowerCase().indexOf(inpt) >= 0 ||
                option.props.name.toLowerCase().indexOf(inpt) >= 0
              );
            }}
          >
            <Input
              style={{ width: 400 }}
              className="input-button-bar"
              onPressEnter={handleButtonClick}
              suffix={
                <Button
                  type="primary"
                  icon="check"
                  onClick={handleButtonClick}
                  style={{
                    borderRadius: '0 4px 4px 0',
                    zIndex: 1,
                    right: 0,
                    margin: 0,
                  }}
                />
              }
            />
          </AutoComplete>
        }
      >
        {children}
      </StyledPopover>
    </>
  );
};

export default AutoCompleteInputPopover;
