import React from 'react';
import { Button } from 'antd';
import styled from 'styled-components';

const StyledButton = styled(Button)`
  padding: 0;
`;

export default function AddButton({ onClick, children }) {
  return (
    <StyledButton type="link" icon="plus" onClick={onClick}>
      {children}
    </StyledButton>
  );
}
