import React from 'react';
import PropTypes from 'prop-types';
import { Popconfirm, Button } from 'antd';

export default function ConfirmActionButton({
  message,
  onConfirm,
  disabled,
  children,
  ...props
}) {
  const button = (
    <Button disabled={disabled} {...props}>
      {children}
    </Button>
  );

  // if button is disabled, don't render the Popconfirm component
  // as it ignores the disabled attribute (making the button still clickable)
  // see https://github.com/ant-design/ant-design/issues/14788
  if (disabled) {
    return button;
  }
  return (
    <Popconfirm
      title={message}
      okText={'yes'}
      cancelText={'no'}
      onConfirm={onConfirm}
    >
      {button}
    </Popconfirm>
  );
}

ConfirmActionButton.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  children: PropTypes.node,
};
