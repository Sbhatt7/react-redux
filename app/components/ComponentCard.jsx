/* eslint-disable react/jsx-props-no-spreading */
import { Card } from 'antd';
import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
  height: 100%;
  overflow: hidden;

  div.overflow-card:hover {
    overflow: overlay;
  }
`;

/**
 * This Component is a classic Card, that is made to contain another component.
 * Therefore its body has no padding and it also handles the scrolling of the
 * content.
 *
 * @param {*} props Will be forwarded to the antd Card component
 *                  (see https://ant.design/components/card/#Card)
 */
const ComponentCard = ({ children, ...cardProps }) => (
  <Card bodyStyle={{ padding: 0 }} {...cardProps}>
    <StyledDiv className="overflow-card">{children}</StyledDiv>
  </Card>
);

export default ComponentCard;
