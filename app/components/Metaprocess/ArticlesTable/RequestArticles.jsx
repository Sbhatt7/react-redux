import { Table, Popover, Modal } from 'antd';
import styled from 'styled-components';
import { AddButton } from 'components/Buttons';
import { useState } from 'react';
import { useRouter } from 'next/router';
import DesignRequest from 'components/DesignRequest';

const WrapDiv = styled.div`
  white-space: pre-wrap;
`;

const StyledTable = styled(Table)`
  .ant-table-footer {
    padding: ${props => props.theme.spacing(1)};

    > * {
      display: block;
    }
  }
`;

const RequestArticles = ({ metaProcess, customerData }) => {
  const { t } = useTranslation('metaprocess');
  const router = useRouter();
  const [modalVisible, setModalVisible] = useState(false);

  const articleData = metaProcess.order.filter(
    order => order.type === 'request',
  )[0].orderitem;

  const columns = [
    {
      title: t('article'),
      dataIndex: 'name',
    },
    {
      title: t('quantityScale'),
      dataIndex: 'quantities',
      align: 'right',
      render: qty => <WrapDiv>{qty}</WrapDiv>,
    },
  ];

  const _data = {};
  articleData.forEach(item => {
    if (_data[item.articleId] === undefined) {
      _data[item.articleId] = {
        id: item.article.id,
        name: item.article.name,
        quantities: [parseInt(item.quantity, 10)],
      };
    } else {
      _data[item.articleId].quantities.push(item.quantity);
    }
  });

  const data = Object.values(_data).map(d => ({
    id: d.id,
    name: d.name,
    quantities: d.quantities.sort().join('\n'),
  }));

  function onAddArticle() {}

  function onAddDevelopment() {
    // router.push('/verkauf/design_request');
    setModalVisible(!modalVisible);
  }

  return (
    <>
      <StyledTable
        rowKey={record => record.id}
        size="middle"
        columns={columns}
        dataSource={data}
        pagination={{ size: 'small', hideOnSinglePage: true }}
        footer={() => (
          <>
            <Popover
              placement="right"
              content="Artikel hinzufügen"
              trigger="click"
            >
              <AddButton>{'addArticle'}</AddButton>
            </Popover>
            <AddButton onClick={onAddDevelopment}>
              {'requestArticleDevelopment'}
            </AddButton>
          </>
        )}
      />
      <Modal
        visible={modalVisible}
        onCancel={() => setModalVisible(false)}
        maskClosable={false}
        width="fit-content"
        footer={null}
      >
        <DesignRequest metaProcess={metaProcess} customerData={customerData} />
      </Modal>
    </>
  );
};

export default RequestArticles;
