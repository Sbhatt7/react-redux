import React from 'react';
import { Rate } from 'antd';

export default function ProfitMarginScoreComponent({ score, average }) {
  const { t } = useTranslation('metaprocess');
  return (
    <>
      <Rate
        disabled
        allowHalf
        value={score}
        style={{ marginTop: -16, marginRight: 8 }}
      />
      {t('customerInfo.averageMargin', {
        margin: average,
      })}
    </>
  );
}
