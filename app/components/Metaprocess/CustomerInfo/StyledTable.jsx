import styled from 'styled-components';

export default styled.table`
  width: 100%;
  border-collapse: collapse;

  td {
    padding: ${props => props.theme.spacing(1)}
      ${props => props.theme.spacing(1)};
    vertical-align: top;
    border-width: 1px 0;
    border-style: solid;
    border-color: #eee;
  }
  tr > td:first-child {
    text-align: end;
    padding-right: 8px;
  }
  tr:first-child > td {
    border-top: none;
  }
  tr:last-child > td {
    border-bottom: none;
  }
`;
