import React from 'react';

export default function LabelRow({ left, right }) {
  return (
    <tr>
      <td>{left}</td>
      <td style={{ whiteSpace: 'pre-wrap' }}>{right}</td>
    </tr>
  );
}
