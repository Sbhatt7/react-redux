import { Empty } from 'antd';
import StyledTable from './StyledTable';
import TwoCols from './TwoCols';
import ProfitMarginScoreComponent from './ProfitMarginScore';

/**
 * Displays customer info
 */
const CustomerInfo = ({ customerData, style }) => {
  const { t } = useTranslation('metaprocess');

  return (
    <div style={style}>
      {customerData.length === 0 && (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}
      <StyledTable>
        <tbody>
          <TwoCols left={'customerInfo.id'} right={customerData.id} />
          <TwoCols left={'customerInfo.name'} right={customerData.name} />
          <TwoCols left={'customerInfo.note'} right={customerData.note} />
          <TwoCols
            left={'customerInfo.profitMargin'}
            right={
              <ProfitMarginScoreComponent
                score={customerData.profitMarginScore}
                average={customerData.averageProfitMargin}
              />
            }
          />
          <TwoCols
            left={'customerInfo.paymentConditions'}
            right={customerData.paymentConditions}
          />
          <TwoCols
            left={'customerInfo.revenueLastYear'}
            right={customerData.revenueLastYear}
          />
        </tbody>
      </StyledTable>
    </div>
  );
};

export default CustomerInfo;
