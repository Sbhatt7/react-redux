import styled from 'styled-components';

const H1 = styled.span`
  font-weight: bold;
  font-size: larger;
`;

const Span = styled.span``;

const Header = ({ metaProcessId, clerk, ...props }) => {
  const { t } = useTranslation('metaprocess');
  return (
    <div {...props}>
      <H1>{'title', { id: metaProcessId }}</H1>
      <Span>{'clerk', { clerk }}</Span>
    </div>
  );
};

export default styled(Header)`
  display: flex;
  justify-content: space-between;
`;
