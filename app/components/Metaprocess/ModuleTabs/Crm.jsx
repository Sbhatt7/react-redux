import { Table, Icon } from 'antd';

// TODO: change this hacky way of making sure the tables have the same width
//       in case of devices with screens <=1024px
const MIN_WIDTH = 545;

const CrmModule = ({ contactData, communicationData, style }) => {
  const communicationColumns = [
    {
      title: 'Datum',
      dataIndex: 'date',
    },
    {
      title: 'Typ',
      dataIndex: 'type',
      width: 40,
      render: type => {
        let iconType = type;
        // fax icon has another name
        if (iconType === 'fax') iconType = 'file-text';
        return <Icon type={iconType} />;
      },
    },
    {
      title: 'Ansprechpartner',
      dataIndex: 'contactName',
    },
    {
      title: 'Kurznotiz',
      dataIndex: 'note',
    },
    {
      title: '',
      dataIndex: 'detailsUrl',
      render: detailsUrl => (
        <a disabled href={detailsUrl}>
          details
        </a>
      ),
    },
  ];

  const contactColumns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Position',
      dataIndex: 'position',
    },
    {
      title: 'E-Mail',
      dataIndex: 'mail',
    },
    {
      title: 'Telefon',
      dataIndex: 'phone',
    },
    {
      title: 'Aktion',
      dataIndex: 'action',
      render: action => (
        <a disabled href={action.url}>
          {action.name}
        </a>
      ),
    },
  ];

  return (
    <div style={{ ...style, height: '100%' }} className="flex-col">
      <Table
        className="flex-grow"
        rowKey={record => record.id}
        size="middle"
        columns={communicationColumns}
        dataSource={communicationData}
        pagination={{ size: 'small', hideOnSinglePage: true }}
        style={{ minWidth: MIN_WIDTH }}
      />
      <Table
        rowKey={record => record.id}
        size="middle"
        columns={contactColumns}
        dataSource={contactData}
        pagination={{ size: 'small', hideOnSinglePage: true }}
        style={{ minWidth: MIN_WIDTH, borderTop: '1px solid #e8e8e8' }}
      />
    </div>
  );
};

CrmModule.defaultProps = {
  communicationData: [
    // {
    //   id: 1,
    //   date: '03.04.2019',
    //   type: 'mail',
    //   contactName: 'Hr. Mustermann',
    //   note: 'Preis zu hoch, möchte neues Angebot',
    //   detailsUrl: '#',
    // },
    // {
    //   id: 2,
    //   date: '01.04.2019',
    //   type: 'phone',
    //   contactName: 'Fr. Mustermann',
    //   note: '-',
    //   detailsUrl: '#',
    // },
  ],
  contactData: [
    {
      id: 1,
      name: 'Max Mustermann',
      position: 'Einkauf',
      mail: 'max.mustermann@email.de',
      phone: '0123 456 78 - 93',
      action: {
        url: '#',
        name: 'E-mail verfassen',
      },
    },
    {
      id: 2,
      name: 'Erika Musterfrau',
      position: 'Geschäftsführung',
      mail: 'erika.musterfrau@email.de',
      phone: '0123 456 78 - 90',
      action: {
        url: '#',
        name: 'E-mail verfassen',
      },
    },
  ],
};

export default CrmModule;
