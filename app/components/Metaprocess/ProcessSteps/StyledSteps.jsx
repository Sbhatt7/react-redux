import styled from 'styled-components';
import { Steps } from 'antd';

export default styled(Steps)`
  margin-top: ${props => props.theme.spacing(2)};
  margin-left: ${props => props.theme.spacing(4)};
  margin-right: ${props => props.theme.spacing(4)};
  margin-bottom: ${props => props.theme.spacing(1)};
  width: calc(100% - ${props => props.theme.spacing(8)});
  overflow-x: auto;

  .ant-steps-item-subtitle {
    display: block;
    line-height: 20px;
    margin: 0 0 8px 0;
    font-weight: normal;
    font-size: 14px;
  }

  &.ant-steps-vertical {
    width: fit-content;
    margin: 0 auto;
  }
`;
