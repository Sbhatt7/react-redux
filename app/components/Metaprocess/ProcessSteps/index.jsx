import { Steps, Icon } from 'antd';
import DeliveryTruck from 'components/Icons/DeliveryTruck';
import Gears from 'components/Icons/Gears';
import { useEffect, useCallback, useState } from 'react';
import StyledSteps from './StyledSteps';

const { Step } = Steps;

const SalesProcessSteps = ({ activePhase }) => {
  const phaseData = [
    {
      id: 0,
      phase: 'request',
      title: 'Kundenanfrage',
      subTitle: 'Eingegangen am 01.04.2019',
      description: '',
      icon: <Icon type="message" />,
    },
    {
      id: 1,
      phase: 'development',
      title: 'Entwicklung',
      subTitle: '',
      description: '',
      icon: <Icon type="bulb" />,
    },
    {
      id: 2,
      phase: 'offer',
      title: 'Verhandlung',
      subTitle: '',
      description: '',
      icon: <Icon type="euro" />,
    },
    {
      id: 3,
      phase: 'order',
      title: 'Zubehörbestellung',
      subTitle: '',
      description: '',
      icon: <Icon type="tool" />,
    },
    {
      id: 4,
      phase: 'production',
      title: 'Fertigung',
      subTitle: '',
      description: '',
      icon: <Gears />,
    },
    {
      id: 5,
      phase: 'request',
      title: 'Versand',
      subTitle: '',
      description: '',
      icon: <DeliveryTruck />,
    },
    {
      id: 6,
      phase: 'order_finished',
      title: 'Fertig',
      subTitle: '',
      description: '',
      icon: <Icon type="check" />,
    },
  ];

  const currentPhaseData = phaseData.find(el => el.phase === activePhase);
  const activePhaseId = currentPhaseData.id;
  const [directionVertical, setDirectionVertical] = useState(false);

  // handler for window resize events to decide whether the steps component
  // should be rendered vertically or horizontally
  const handleWindowResize = useCallback(() => {
    if (window.innerWidth < 992 && !directionVertical) {
      setDirectionVertical(true);
    } else if (window.innerWidth >= 992 && directionVertical) {
      setDirectionVertical(false);
    }
  }, [directionVertical]);

  useEffect(() => {
    window.addEventListener('resize', handleWindowResize);
    // call the function once to determine the initial direction
    handleWindowResize();

    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, [handleWindowResize]);

  return (
    <>
      <StyledSteps
        labelPlacement={directionVertical ? 'horizontal' : 'vertical'}
        direction={directionVertical ? 'vertical' : 'horizontal'}
        current={activePhaseId}
      >
        {phaseData.map(p => (
          <Step
            key={p.id}
            title={p.title}
            subTitle={p.id === activePhaseId ? p.subTitle : ''}
            description={p.id === activePhaseId ? p.description : ''}
            icon={p.icon}
          />
        ))}
      </StyledSteps>
    </>
  );
};

export default SalesProcessSteps;
