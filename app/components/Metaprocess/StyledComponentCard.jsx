import styled from 'styled-components';
import ComponentCard from 'components/ComponentCard';

export default styled(ComponentCard)`
  margin-top: ${props => props.theme.spacing(2)};
`;
