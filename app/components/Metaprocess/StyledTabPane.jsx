import styled from 'styled-components';
import { Tabs } from 'antd';

export default styled(Tabs.TabPane)`
  &.ant-tabs-tabpane-active {
    height: calc(100% - 43px);
  }
`;
