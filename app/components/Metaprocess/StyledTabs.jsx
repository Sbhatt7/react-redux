import styled from 'styled-components';
import { Tabs } from 'antd';

const MyTabs = props => (
  <Tabs type="line" animated={false} tabBarStyle={{ margin: 0 }} {...props} />
);

export default styled(MyTabs)`
  height: 100%;

  .ant-tabs-content {
    height: 100%;
    overflow-x: auto;
  }
`;
