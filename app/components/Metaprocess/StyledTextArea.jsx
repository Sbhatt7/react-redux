import styled from 'styled-components';
import { Input } from 'antd';

export default styled(Input.TextArea)`
  margin: ${props => props.theme.spacing(1)};
  width: calc(100% - ${props => props.theme.spacing(2)});
  resize: none;
`;
