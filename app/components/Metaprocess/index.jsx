// import Request from './Request';

import React, { useCallback, useState, useEffect } from 'react';
import { Col, Row, Divider } from 'antd';
import StyledTextArea from 'components/Metaprocess/StyledTextArea';
import { useTheme } from 'styled-components';
import Header from 'components/Metaprocess/Header';
import StyledComponentCard from './StyledComponentCard';
import CustomerInfo from './CustomerInfo';
import CrmModule from './ModuleTabs/Crm';
import ProcessSteps from './ProcessSteps';
import StyledTabs from './StyledTabs';
import DevelopmentModule from './ModuleTabs/Development';
import OfferModule from './ModuleTabs/Offer';
import OrderModule from './ModuleTabs/Order';
import StyledTabPane from './StyledTabPane';
import RequestArticles from './ArticlesTable/RequestArticles';

function MetaProcess({ metaProcess, customerData }) {
  const [isMobile, setIsMobile] = useState(false);
  const { t } = useTranslation();
  const theme = useTheme();

  // hande window resizte for responsive layout
  const handleWindowResize = useCallback(() => {
    if (window.innerWidth < 992 && !isMobile) {
      setIsMobile(true);
    } else if (window.innerWidth >= 992 && isMobile) {
      setIsMobile(false);
    }
  }, [isMobile]);

  useEffect(() => {
    window.addEventListener('resize', handleWindowResize);
    // call the function to determine the initial direction
    handleWindowResize();

    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, [handleWindowResize]);

  const phaseComponents = [
    {
      id: 0,
      phase: 'crm',
      name: t('kundengespräche'),
      component: <CrmModule />,
    },
    { id: 1, phase: 'request', component: null },
    {
      id: 2,
      phase: 'development',
      name: t('entwicklung'),
      component: <DevelopmentModule />,
    },
    { id: 3, phase: 'offer', name: t('offer'), component: <OfferModule /> },
    { id: 4, phase: 'order', name: t('order'), component: <OrderModule /> },
  ];

  const currentPhaseId = phaseComponents.filter(
    p => p.phase === metaProcess.phase,
  )[0].id;

  const tabPanes = phaseComponents.map(comp => {
    if (comp.id > currentPhaseId || comp.component === null) return false;
    return (
      <StyledTabPane tab={comp.name} key={comp.phase}>
        {comp.component}
      </StyledTabPane>
    );
  });

  // TODO: when in phase order for example, the article data should
  //       be fetched from the orde rwith type === 'order' etc.
  const articleData = metaProcess.order.filter(
    order => order.type === 'request',
  )[0].orderitem;

  const gutter = theme.gutter(2);

  // mobile layout
  if (isMobile) {
    return (
      <>
        <Header metaProcessId={metaProcess.id} clerk="Max Mustermann" />
        <Row gutter={gutter} className="flex-row">
          <Col xs={24} md={10}>
            <StyledComponentCard>
              <ProcessSteps activePhase={metaProcess.phase} />
            </StyledComponentCard>
          </Col>
          <Col xs={24} md={14} className="flex-grow">
            <StyledComponentCard>
              <CustomerInfo customerData={customerData} />
              <Divider style={{ margin: 0 }} />
              <StyledTextArea placeholder={'note'} />
            </StyledComponentCard>
          </Col>
          <Col span={24}>
            <StyledComponentCard>
              <RequestArticles
                metaProcess={metaProcess}
                customerData={customerData}
              />
            </StyledComponentCard>
          </Col>
          <Col span={24}>
            <StyledComponentCard>
              <StyledTabs defaultActiveKey="0">{tabPanes}</StyledTabs>
            </StyledComponentCard>
          </Col>
        </Row>
      </>
    );
  }

  // standard desktop layout
  return (
    <>
      <Header metaProcessId={metaProcess.id} clerk="Max Mustermann" />
      <Row gutter={gutter}>
        <Col span={24}>
          <StyledComponentCard>
            <ProcessSteps activePhase={metaProcess.phase} />
          </StyledComponentCard>
        </Col>
      </Row>
      <Row gutter={gutter} className="flex-row flex-grow">
        <Col xs={24} md={11} lg={11} xl={9} className="flex-col">
          <StyledComponentCard>
            <CustomerInfo customerData={customerData} />
            <Divider style={{ margin: 0 }} />
            <StyledTextArea placeholder={'note'} />
          </StyledComponentCard>
          <StyledComponentCard className="flex-grow">
            <RequestArticles
              metaProcess={metaProcess}
              customerData={customerData}
            />
          </StyledComponentCard>
        </Col>
        <Col xs={24} md={13} lg={13} xl={15} className="flex-col">
          <StyledComponentCard className="flex-grow">
            <StyledTabs defaultActiveKey="0">{tabPanes}</StyledTabs>
          </StyledComponentCard>
        </Col>
      </Row>
    </>
  );
}

export default MetaProcess;
