import PropTypes from 'prop-types';
import React, { memo } from 'react';
import styled from 'styled-components';

const Path = styled.path`
  fill: ${props => props.theme.colors.primary};
  stroke: #ee711b;
  stroke-width: 0px;
`;
const Svg = styled.svg`
  :hover {
    transform-origin: center;
    transform: scale(1.01);
    cursor: pointer;
  }
  :hover path {
    fill: transparent;
    stroke: #ee711b;
    stroke-width: 0.8px;
    transition: 0.04s ease-in-out;
  }
  :active {
    transform-origin: center;
    transform: scale(0.98);
  }
  :active path {
    fill: #ee711b;
    opacity: 0.7;
    stroke-width: 0px;
  }
  div,
  path {
    transition: 0.15s ease-in-out;
  }
`;

function PantherLogo({ onClick, caption, ...props }) {
  return (
    <>
      <div {...props}>
        <Svg
          onClick={onClick}
          viewBox="-1 -1 80 104"
          version="1.1"
          id="logo-panther-packaging"
          xmlns="http://www.w3.org/2000/svg"
          xmlns-xlink="http://www.w3.org/1999/xlink"
          x="0px"
          y="0px"
          xml-space="preserve"
        >
          <Path d="M78.375,35.325l0,14.175l-11.25,0c-4.2,0 -8.25,1.2 -10.725,2.475c-2.475,1.2 -5.1,3.9 -7.05,5.625c-1.95,1.8 -43.2,43.575 -43.2,43.575l0,-71.4c0,0 -2.1,0.15 -4.05,-1.2c-1.95,-1.425 -2.1,-3.9 -2.1,-3.9c0,0 1.5,-0.15 2.625,-0.675c1.125,-0.525 3.075,-2.025 4.275,-3c1.125,-0.975 9.45,-10.05 9.45,-10.05c3.45,-3.15 9.825,-10.95 23.925,-10.95c9.3,0 14.475,3.45 16.95,5.85l21.15,0l0,24.45l-4.725,-4.65l0,-2.025c0,0 -7.275,-0.6 -12.75,4.875c-5.475,5.475 -6.075,11.7 -6.075,11.7l19.05,0l4.5,-4.875Zm-24.6,-11.325l14.325,-5.925l0,-1.05l-15.675,4.95l1.35,2.025Zm-2.1,-3.375l16.425,-4.875l0,-1.05l-17.7,3.975l1.275,1.95Zm-2.175,-3.375l18.6,-3.825l0,-1.05l-19.725,3l1.125,1.875Zm-3.9,-6l6.15,1.425l1.275,-3.15l-7.275,1.2l-0.15,0.525Z" />
        </Svg>
        {caption}
      </div>
    </>
  );
}

PantherLogo.propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  onClick: PropTypes.func,
  caption: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
};

export default memo(PantherLogo);
