import React, { memo } from 'react';
import styled from 'styled-components';
import PantherLogo from './BaseLogo';

const Div = styled.div`
  text-align: center;
  cursor: default;
  user-select: none;
`;
const P = styled.p`
  margin: 0;
  margin-top: -4px;
`;
const B = styled.b`
  color: ${props => props.theme.colors.primary};
`;
const Small = styled.small`
  font-size: 0.9em;
  margin-top: -4px;
`;

function PantherLogoWithVersion(props) {
  return (
    <PantherLogo
      {...props}
      caption={
        <Div>
          <P>
            <B>{process.env.APP_NAME}</B>
          </P>
          <P>
            <Small>{process.env.APP_VERSION}</Small>
          </P>
        </Div>
      }
    />
  );
}

export default memo(PantherLogoWithVersion);
