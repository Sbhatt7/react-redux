/**
 *
 * PantherLogo
 *
 */
import PantherLogo from './BaseLogo';
import PantherLogoWithVersion from './LogoWithVersion';

export { PantherLogo, PantherLogoWithVersion };

export default PantherLogo;
