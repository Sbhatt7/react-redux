import styled from 'styled-components';

const LinkItem = styled.a`
  width: 100%;
  margin: 0 auto;
  display: block;
`;

export default LinkItem;
