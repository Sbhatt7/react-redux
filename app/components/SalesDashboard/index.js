/**
 *
 * Dashboard
 *
 */

import React from 'react';
import { Card, Col, Row, Tabs, Table } from 'antd';
import ComponentCard from 'components/ComponentCard';
import AutoCompleteInputPopover from 'components/AutoCompleteInputPopover';
import { useTheme } from 'styled-components';
import LinkItem from './LinkItem';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Dashboard() {
  const theme = useTheme();
  return (
    // <div>test</div>
    <Row gutter={theme.gutter(2)} type="flex" style={{ height: '100%' }}>
      <Col span={16} className="flex-col">
        <Card bodyStyle={{ marginBottom: 32 }}>
          <Col className="gutter-row" span={6}>
            <h2>Kundenverwaltung</h2>
            <LinkItem disabled>Neuer Kunde</LinkItem>
            <LinkItem disabled>Kunden bearbeiten</LinkItem>
          </Col>
          <Col className="gutter-row" span={6}>
            <h2>Artikelverwaltung</h2>
            <LinkItem disabled>Neuer Artikel</LinkItem>
            <LinkItem disabled>Artikel bearbeiten</LinkItem>
          </Col>
          <Col className="gutter-row" span={6}>
            <h2>Auftragsverwaltung</h2>
            <LinkItem href="/verkauf/vorgang/new">Neue Kundenanfrage</LinkItem>
            <AutoCompleteInputPopover
              onSubmit={
                id => console.log(id)
                // router.push('/verkauf/vorgang/[id]', `/verkauf/vorgang/${id}`)
              }
              suggestions={[]}
            >
              <LinkItem>Vorgang bearbeiten</LinkItem>
            </AutoCompleteInputPopover>
          </Col>
          <Col className="gutter-row" span={6}>
            <h2>Lagerverwaltung</h2>
            <LinkItem disabled>Lagerbestände anzeigen</LinkItem>
          </Col>
        </Card>
        <ComponentCard
          className="flex-grow"
          style={{ marginTop: theme.spacing(2) }}
        >
          <Tabs
            type="line"
            defaultActiveKey="1"
            animated={false}
            tabBarStyle={{ margin: 0 }}
          >
            <Tabs.TabPane tab="Offene Bestellungen" key="1">
              <Table
                // columns={columns}
                // dataSource={data}
                pagination={{ size: 'small', hideOnSinglePage: true }}
              />
            </Tabs.TabPane>
            <Tabs.TabPane tab="Beantragte Entwicklungen" key="2">
              Content of Tab Pane 2
            </Tabs.TabPane>
            <Tabs.TabPane tab="Aktueller Produktionsplan" key="3">
              Content of Tab Pane 3
            </Tabs.TabPane>
          </Tabs>
        </ComponentCard>
      </Col>
      <Col span={8} className="flex-col">
        <ComponentCard className="flex-grow">
          {/* <Reminder /> */}
        </ComponentCard>
      </Col>
    </Row>
  );
}

// Dashboard.propTypes = {

// };

export default Dashboard;
