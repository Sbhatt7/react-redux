import React from 'react';
import { Radio } from 'antd';

export default function UserSettings() {
  return (
    <>
      <p>{('settings.role', 'someone')}</p>
      {'settings.changeLocale'}
      :
      <br />
      <Radio.Group
        // onChange={e => i18n.changeLanguage(e.target.value)}
        defaultValue="de"
      >
        <Radio.Button value="de">Deutsch (DE)</Radio.Button>
        <Radio.Button value="en">English (EN)</Radio.Button>
      </Radio.Group>
    </>
  );
}
