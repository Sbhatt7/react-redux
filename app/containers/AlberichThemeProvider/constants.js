/*
 * AlberichThemeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_THEME = 'alberich/AppTheme/CHANGE_THEME';

export const font = {
  FONT_SIZE_TINY: 8,
  FONT_SIZE_SMALL: 12,
  FONT_SIZE_MEDIUM: 14,
  FONT_SIZE_LARGE: 18,
  FONT_SIZE_EXTRA_LARGE: 24,
  FONT_SIZE_MASSIVE: 34,

  FONT_WEIGHT_LIGHT: '200',
  FONT_WEIGHT_MEDIUM: '500',
  FONT_WEIGHT_BOLD: '700',

  PRIMARY_FONT_FAMILY: 'AvertaDemo-Regular',
  PRIMARY_FONT_FAMILY_BOLD: 'AvertaDemo-ExtraBoldItalic',

  SECONDARY_FONT_FAMILY: 'Product-Sans-Regular',
  SECONDARY_FONT_FAMILY_ITALIC: 'Product-Sans-Italic',
};

export const spacing = {
  FONT_SIZE_TINY: 8,
  FONT_SIZE_SMALL: 12,
  FONT_SIZE_MEDIUM: 14,
  FONT_SIZE_LARGE: 18,
  FONT_SIZE_EXTRA_LARGE: 24,
  FONT_SIZE_MASSIVE: 34,

  FONT_WEIGHT_LIGHT: '200',
  FONT_WEIGHT_MEDIUM: '500',
  FONT_WEIGHT_BOLD: '700',

  PRIMARY_FONT_FAMILY: 'AvertaDemo-Regular',
  PRIMARY_FONT_FAMILY_BOLD: 'AvertaDemo-ExtraBoldItalic',

  SECONDARY_FONT_FAMILY: 'Product-Sans-Regular',
  SECONDARY_FONT_FAMILY_ITALIC: 'Product-Sans-Italic',
};

export const darkTheme = {
  PRIMARY_BACKGROUND_COLOR: '#3d3d3d',
  PRIMARY_BACKGROUND_COLOR_LIGHT: '#797979',

  SECONDARY_BACKGROUND_COLOR: '#ffffff',
  SECONDARY_BACKGROUND_COLOR_LIGHT: '#f7f7f7',

  PRIMARY_TEXT_COLOR: '#ffffff',
  PRIMARY_TEXT_COLOR_LIGHT: '#f7f7f7',
  SECONDARY_TEXT_COLOR: '#3d3d3d',
  PRIMARY_TEXT_BACKGROUND_COLOR: '#3d3d3d',
  SECONDARY_TEXT_BACKGROUND_COLOR: '#ffffff',
};
export const lightTheme = {
  PRIMARY_BACKGROUND_COLOR: '#ffffff',
  PRIMARY_BACKGROUND_COLOR_LIGHT: '#f7f7f7',

  SECONDARY_BACKGROUND_COLOR: '#3d3d3d',
  SECONDARY_BACKGROUND_COLOR_LIGHT: '#797979',

  primary: '#ee711b',
  PRIMARY_TEXT_COLOR_LIGHT: '#797979',
  SECONDARY_TEXT_COLOR: '#ffffff',
  PRIMARY_TEXT_BACKGROUND_COLOR: '#ffffff',
  SECONDARY_TEXT_BACKGROUND_COLOR: '#3d3d3d',
};
