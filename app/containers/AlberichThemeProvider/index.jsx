import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { ThemeProvider } from 'styled-components';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';
import { makeSelectTheme } from './selectors';

const key = 'theme';

const Theme = ({ theme, children }) => {
  useInjectReducer({ key, reducer });
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

const mapStateToProps = createStructuredSelector({
  theme: makeSelectTheme(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(Theme);
