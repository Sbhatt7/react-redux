/*
 * AlberichThemeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { CHANGE_THEME } from './constants';

// The initial state of the App
const SPACING_BASE = 8;
const SPACING_UNIT = 'px';

// const FONT_SIZE_BASE = 14;
// const FONT_SIZE_HEADING = FONT_SIZE_BASE + 6;
// const FONT_SIZE_UNIT = 'px';

// card box-shadow: 0px 6px 8px 0px #00000014;

export const initialState = {
  colors: {
    background: '#ffffff',
    backgroundLayout: '#fafafa',
    primary: '#EE711B',
  },
  spacing: x => `${x * SPACING_BASE}${SPACING_UNIT}`,
  spacingNum: x => x * SPACING_BASE,
  gutter: (x, y) => [x * SPACING_BASE, y !== undefined ? y * SPACING_BASE : 0],
};

/* eslint-disable default-case, no-param-reassign */
const themeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CHANGE_THEME: {
        const keys = Object.keys(action.theme);
        for (let i = 0; i < keys.length; i += 1) {
          const key = keys[i];
          draft[key] = action.theme[key];
        }
        break;
      }
    }
  });

export default themeReducer;
