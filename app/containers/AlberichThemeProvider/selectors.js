import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTheme = state => state.theme || initialState;

const makeSelectTheme = () =>
  createSelector(
    selectTheme,
    themeState => themeState,
  );

export { makeSelectTheme };
