/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

 /* eslint-disable*/
import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import SalesDashboard from 'components/SalesDashboard';

import SidebarLayout from 'containers/SidebarLayout';

import { useInjectReducer } from 'utils/injectReducer';
import userReducer from 'containers/User/reducer';
import globalReducer from 'containers/Global/reducer';

import '../../styles.less';
import Dashboard from 'containers/DevelopmentDashboard';

const AppWrapper = styled.div``;

// wrap a component with this HOC to render it with a sidebar
const withSidebar = Component => props => {
  console.log("props", props);
  return (
    (
      <SidebarLayout>
        <Component {...props} />
      </SidebarLayout>
    )
  )
};

export default function App() {
  useInjectReducer({ key: 'global', reducer: globalReducer });
  useInjectReducer({ key: 'user', reducer: userReducer });

  return (
    <AppWrapper>
      <Helmet titleTemplate="%s - React.js Boilerplate" defaultTitle="Alberich">
        <meta name="description" content="ERP" />
      </Helmet>
      <Switch>
        <Route exact path="/" component={withSidebar(SalesDashboard)} />
        <Route exact path="/development/dashboard" component={withSidebar(Dashboard)} />
        <Route path="" component={NotFoundPage} />
      </Switch>
    </AppWrapper>
  );
}
