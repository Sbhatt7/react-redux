import React from 'react';
import styled, { useTheme } from 'styled-components';
import { Row } from 'antd';

// eslint-disable-next-line react/prop-types
const MyRow = ({ children, ...props }) => {
  const theme = useTheme();
  return (
    <Row gutter={theme.gutter(2)} type="flex" {...props}>
      {children}
    </Row>
  );
};

const Content = styled(MyRow)`
  height: 100%;
`;

export default Content;
