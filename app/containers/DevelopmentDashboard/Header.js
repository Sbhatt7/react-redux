import styled from 'styled-components';
import { Row } from 'antd';

const Header = styled(Row)`
  margin-bottom: ${props => props.theme.spacing(1)};
`;

export default Header;
