/**
 *
 * Dashboard
 *
 */
 /* eslint-disable */

import React from 'react';
import { Col, Table } from 'antd';
import ComponentCard from 'components/ComponentCard';
import { useTheme } from 'styled-components';
import { FormattedMessage } from 'react-intl';
import {
  makeSelectReminders, makeSelectUser,
  makeSelectDesignRequests,
} from 'containers/User/selectors';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import messages from './messages';
import Content from './Content';
import Header from './Header';

// import PropTypes from 'prop-types';

function Dashboard({ reminders, designRequests }) {
  const theme = useTheme();
  const designColumns = [
      {
          title: "Id",
          key: "id",
          render: (text, item, index) => {
              return (
                  <div>
                      <span>{item.id}</span>
                  </div>
              );
          }
      },
      {
          title: "Clerk",
          key: "clerk",
          render: (text, item, index) => {
              return (
                  <div>
                      <span>{item.clerk}</span>
                  </div>
              );
          }
      },
      {
          title: "Type",
          key: "type",
          render: (text, item, index) => {
              return (
                  <div>
                      <span>{item.type}</span>
                  </div>
              );
          }
      },
      {
          title: "Note",
          key: "note",
          render: (text, item, index) => {
              return (
                  <div>
                      <span>{item.note}</span>
                  </div>
              );
          }
      },
      {
          title: "Action",
          key: "action",
          render: (text, item, index) => {
              return (
                  <div>
                      <a href={item.href}>
                        Edit
                      </a>
                  </div>
              );
          }
      },
  ];

  const reminderColumns = [
    {
        title: "Id",
        key: "id",
        render: (text, item, index) => {
            return (
                <div>
                    <span>{item.id}</span>
                </div>
            );
        }
    },
    {
        title: "Date",
        key: "date",
        render: (text, item, index) => {
            return (
                <div>
                    <span>{item.date}</span>
                </div>
            );
        }
    },
    {
        title: "Note",
        key: "note",
        render: (text, item, index) => {
            return (
                <div>
                    <span>{item.note}</span><a href={item.href}>link</a>
                </div>
            );
        }
    },
  ];

  console.log('Data for Reminder Table', reminders);
  console.log('Data for Design Request Table', designRequests);

  return (
    <>
      <Header style={{ marginBottom: theme.spacing(1) }}>
        <FormattedMessage {...messages.title} />
      </Header>
      <Content>
        <Col className="flex-col">
          <ComponentCard className="flex-grow">
            {/* Use Data from designRequests */}
            {/* Docs for Table: https://ant.design/components/table */}
            <Table
              columns={designColumns}
              dataSource={designRequests}
              pagination={{ size: 'small', hideOnSinglePage: true }}
            />
          </ComponentCard>
        </Col>
        <Col className="flex-col">
          <ComponentCard>
            {/* Use Data from reminders */}
            <Table
              columns={reminderColumns}
              dataSource={reminders}
              pagination={{ size: 'small', hideOnSinglePage: true }}
            />
          </ComponentCard>
        </Col>
      </Content>
    </>
  );
}

// Dashboard.propTypes = {

// };

const mapStateToProps = createStructuredSelector({
  reminders: makeSelectReminders(),
  designRequests: makeSelectDesignRequests(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(Dashboard);
