/*
 * DevelopmentDashboard Messages
 *
 * This contains all the text for the DevelopmentDashboard container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.DevelopmentDashboard';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'This is the DevelopmentDashboard container!',
  },
});
