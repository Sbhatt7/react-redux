import { SAVE, DISCARD_CHANGES, SET_LAYOUT_EDITABLE } from './constants';

/**
 * Set the LayoutEditable variable to the given bool value
 */
export function setLayoutEditable(layoutEditable) {
  return {
    type: SET_LAYOUT_EDITABLE,
    layoutEditable,
  };
}

/**
 * Save all content on the current page, which changed
 */
export function save() {
  return {
    type: SAVE,
  };
}

export function discardUnsavedChanges() {
  return {
    type: DISCARD_CHANGES,
  };
}
