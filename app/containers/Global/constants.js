export const SAVE = 'alberich/Global/SAVE';
export const DISCARD_CHANGES = 'alberich/Global/DISCARD_CHANGES';
export const DATA_CHANGED = 'alberich/Global/DATA_CHANGED';
export const SET_LAYOUT_EDITABLE = 'alberich/Global/SET_LAYOUT_EDITABLE';
