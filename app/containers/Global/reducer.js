/*
 * GlobalReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  SAVE,
  DISCARD_CHANGES,
  SET_LAYOUT_EDITABLE,
  DATA_CHANGED,
} from './constants';

export const initialState = {
  layoutEditable: false,
  unsavedChanges: false,
};

/* eslint-disable default-case, no-param-reassign */
const globalReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET_LAYOUT_EDITABLE:
        draft.layoutEditable = action.layoutEditable;
        break;
      case DATA_CHANGED:
        draft.unsavedChanges = true;
        break;
      case DISCARD_CHANGES:
        draft.unsavedChanges = false;
        break;
      case SAVE:
        draft.unsavedChanges = false;
        break;
    }
  });

export default globalReducer;
