import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectGlobal = state => state.global || initialState;

const makeSelectLayoutEditable = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.layoutEditable,
  );

const makeSelectUnsavedChanges = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.unsavedChanges,
  );

export { makeSelectLayoutEditable, makeSelectUnsavedChanges };
