import { Layout } from 'antd';
import styled from 'styled-components';

const ContentLayout = styled(Layout)`
  background: ${props => props.theme.colors.backgroundLayout} !important;
  padding: ${props => props.theme.spacing(2)};
`;

export default ContentLayout;
