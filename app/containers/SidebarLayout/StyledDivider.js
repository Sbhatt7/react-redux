import styled from 'styled-components';
import { Divider } from 'antd';

const StyledDivider = styled(Divider)`
  margin-bottom: 0;
`;

export default StyledDivider;
