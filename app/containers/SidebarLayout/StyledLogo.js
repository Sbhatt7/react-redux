import { PantherLogoWithVersion } from 'components/PantherLogo';
import styled from 'styled-components';

const StyledLogo = styled(PantherLogoWithVersion)`
  margin: ${props => props.theme.spacing(1)};
  margin-bottom: 0;
`;

export default StyledLogo;
