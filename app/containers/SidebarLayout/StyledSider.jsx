import styled from 'styled-components';
import { Layout } from 'antd';
import React from 'react';

const { Sider } = Layout;
const lightSider = props => <Sider theme="light" {...props} />;

export default styled(lightSider)`
  height: 100vh;
  position: fixed !important;
  z-index: 999;
`;
