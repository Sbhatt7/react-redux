import React from 'react';
import UserSettings from 'components/UserSettings';
import PropTypes from 'prop-types';
import { Button, Modal } from 'antd';

export default function UserSettingsModal({ isVisible, setVisible }) {
  return (
    <Modal
      visible={isVisible}
      title="settings"
      onCancel={() => setVisible(false)}
      zIndex={1100}
      footer={[
        <Button
          key="done"
          type="primary"
          onClick={() => setVisible(!isVisible)}
        >
          {'done'}
        </Button>,
      ]}
    >
      <UserSettings />
    </Modal>
  );
}

UserSettingsModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  setVisible: PropTypes.func.isRequired,
};
