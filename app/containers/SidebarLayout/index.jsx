/**
 *
 * SidebarLayout
 *
 */

import { Icon, Layout, Menu } from 'antd';
import { save } from 'containers/Global/actions';
import { makeSelectUnsavedChanges } from 'containers/Global/selectors';
import PropTypes from 'prop-types';
import React, { memo, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { FormattedMessage } from 'react-intl';
import ContentLayout from './ContentLayout';
import StyledLogo from './StyledLogo';
import StyledSider from './StyledSider';
import UserSettingsModal from './UserSettingsModal';
import StyledDivider from './StyledDivider';

import messages from './messages';

const { Sider } = Layout;

function SidebarLayout({ unsavedChanges, onSave, children }) {
  const [collapsed, setCollapsed] = useState(true);
  const [spotlightVisible, setSpotlightVisible] = useState(false);
  const [settingsVisible, setSettingsVisible] = useState(false);

  useEffect(() => {
    const saveFunction = event => {
      if (event.ctrlKey && event.key === 's') {
        event.preventDefault();
        if (unsavedChanges) onSave();
      }
    };

    document.addEventListener('keydown', saveFunction, false);
    return () => {
      document.removeEventListener('keydown', saveFunction, false);
    };
  }, [unsavedChanges]);

  const goToHomepage = () => {
    // router.push('/');
  };

  return (
    <>
      <Layout hasSider style={{ minHeight: '100vh' }}>
        <StyledSider
          collapsible
          collapsed={collapsed}
          onCollapse={setCollapsed}
        >
          <StyledLogo onClick={goToHomepage} />
          <StyledDivider />
          <Menu defaultSelectedKeys={[]} mode="inline" selectable={false}>
            <Menu.Item
              key="1"
              onClick={() => setSpotlightVisible(!spotlightVisible)}
            >
              <Icon type="search" />
              <FormattedMessage {...messages.search} />
            </Menu.Item>
            <Menu.Item
              key="2"
              onClick={() => setSettingsVisible(!settingsVisible)}
            >
              <Icon type="setting" />
              <FormattedMessage {...messages.settings} />
            </Menu.Item>
            <Menu.Item key="3" disabled={!unsavedChanges} onClick={onSave}>
              <Icon type="save" />
              <span className="__alberich-save">
                <FormattedMessage {...messages.save} />
              </span>
            </Menu.Item>
          </Menu>
        </StyledSider>
        <UserSettingsModal
          isVisible={settingsVisible}
          setVisible={setSettingsVisible}
        />
        {/* only here to make the callapsing work as expected
            it's rendered below the StyledSider component */}
        <Sider collapsible collapsed={collapsed} style={{ opacity: 0 }} />
        <ContentLayout>{children}</ContentLayout>
      </Layout>
    </>
  );
}

SidebarLayout.propTypes = {
  unsavedChanges: PropTypes.bool,
  onSave: PropTypes.func.isRequired,
  children: PropTypes.node,
};

const mapStateToProps = createStructuredSelector({
  unsavedChanges: makeSelectUnsavedChanges(),
});

function mapDispatchToProps(dispatch) {
  return {
    onSave: () => {
      // fix tooltip being displayed forever after disabling menu item
      const tooltipList = document.getElementsByClassName(
        'ant-tooltip ant-menu-inline-collapsed-tooltip ant-tooltip-placement-right',
      );
      for (let i = 0; i < tooltipList.length; i += 1) {
        const el = tooltipList[i];
        if (el.getElementsByClassName('__alberich-save').length > 0) {
          el.classList.add('ant-tooltip-hidden');
        }
      }
      dispatch(save());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SidebarLayout);
