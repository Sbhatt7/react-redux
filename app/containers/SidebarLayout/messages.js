/*
 * Sidebar Messages
 *
 * This contains all the text for the Sidebar component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Sidebar';

export default defineMessages({
  search: {
    id: `${scope}.search`,
    defaultMessage: 'Suchen...',
  },
  settings: {
    id: `${scope}.settings`,
    defaultMessage: 'Einstellungen',
  },
  save: {
    id: `${scope}.save`,
    defaultMessage: 'Speichern',
  },
});
