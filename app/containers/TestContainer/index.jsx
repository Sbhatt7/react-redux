import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { Form, Input, Button } from 'antd';
import { changeData } from 'containers/TestContainer/actions';
import { makeSelectUnsavedChanges } from 'containers/Global/selectors';
import reducer from './reducer';
import { makeSelectTheme } from './selectors';

const key = 'test';

const Theme = ({ form, theme, children, onDataChange, unsavedChanges }) => {
  useInjectReducer({ key, reducer });
  const { getFieldDecorator } = form;

  function handleSubmit(e) {
    e.preventDefault();
    console.log(e);
  }

  function onFormDataChange() {
    if (!unsavedChanges) onDataChange();
  }

  return (
    <div>
      <p>Okay this is a test. Your Theme:</p>
      <p>{JSON.stringify(theme)}</p>
      {children}
      <Form layout="inline" onSubmit={handleSubmit}>
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(<Input placeholder="Username" onChange={onFormDataChange} />)}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Log in
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  theme: makeSelectTheme(),
  unsavedChanges: makeSelectUnsavedChanges(),
});

function mapDispatchToProps(dispatch) {
  return {
    onDataChange: () => dispatch(changeData()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  Form.create({ name: 'test_form' }),
  withConnect,
)(Theme);
