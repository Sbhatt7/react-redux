/*
 * AlberichThemeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { SAVE } from 'containers/Global/constants';

export const initialState = {};

/* eslint-disable default-case, no-param-reassign */
const themeReducer = (state = initialState, action) =>
  produce(state, () => {
    switch (action.type) {
      case SAVE:
        console.log('oh, we should save!');
        break;
    }
  });

export default themeReducer;
