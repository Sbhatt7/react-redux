import { SET_HOME } from './constants';

/**
 * Save all content on the current page, which changed
 */
export function setLayoutEditable(path) {
  return {
    type: SET_HOME,
    path,
  };
}
