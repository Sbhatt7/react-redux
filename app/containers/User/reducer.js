/*
 * GlobalReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import { SET_HOME } from './constants';

export const initialState = {
  home: '/',
  designRequests: [
    {
      id: '123456',
      clerk: 'Max Mustermann',
      type: 'Muster',
      note: 'Neuentwicklung für Intersnack',
      href: '/design_requests/123456',
    },
    {
      id: '234567',
      clerk: 'Max Mustermann',
      type: 'Scribble/Grafik',
      note: '3D Bild von 2 Seiten erstellen',
      href: '/design_requests/234567',
    },
    {
      id: '345678',
      clerk: 'Max Mustermann',
      type: 'Ausrechnung',
      note: 'Kostenberechnung für 1/8 Display',
      href: '/design_requests/345678',
    },
  ],
  reminders: [
    {
      id: '1',
      date: '23.09.2019',
      note: 'Neuentwicklung für Kunde Intersnack abschließen',
      href: '/design_requests/123456',
    },
    {
      id: '2',
      date: '24.09.2019',
      note: 'Werkzeugbestellung 7483934 checken',
      href: '/order/7483934',
    },
  ],
};

/* eslint-disable default-case, no-param-reassign */
const userReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET_HOME:
        draft.home = action.path;
        break;
    }
  });

export default userReducer;
