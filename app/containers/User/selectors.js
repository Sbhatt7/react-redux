import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectUser = state => state.user || initialState;

const makeSelectHome = () =>
  createSelector(
    selectUser,
    state => state.home,
  );

const makeSelectReminders = () =>
  createSelector(
    selectUser,
    state => state.reminders,
  );

// eslint-disable-next-line no-unused-vars
const makeSelectDesignRequests = () =>
  createSelector(
    selectUser,
    state => state.designRequests,
  );

// const makeSelectDesignRequests =

export { makeSelectHome, makeSelectReminders, makeSelectDesignRequests };
